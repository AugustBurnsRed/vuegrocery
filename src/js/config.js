import Firebase from 'firebase'
var config = {
  apiKey: 'AIzaSyCtq0vLgoX7edwkYn963V0MI1WE4sAx7q0',
  authDomain: 'calendar-b8e3f.firebaseapp.com',
  databaseURL: 'https://calendar-b8e3f.firebaseio.com',
  storageBucket: 'calendar-b8e3f.appspot.com'
}
// var firebase = Firebase.initializeApp(config)
var firebase = Firebase.initializeApp(config)
/* eslint-disable no-unused-vars*/
var db = firebase.database()

var provider = new Firebase.auth.GoogleAuthProvider()

export default {
  db: db,
  firebase: firebase
}
