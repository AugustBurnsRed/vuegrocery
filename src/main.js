import Vue from 'vue'
import VueRouter from 'vue-router'
import configureRoutes from './routes'
import Sortable from 'vue-sortable'

// Initialize Firebase

Vue.use(Sortable)
Vue.use(VueRouter)

const router = new VueRouter({
  history: true,
  saveScrollPosition: true
})
configureRoutes(router)

// boostrap the app
const App = Vue.extend(require('./App.vue'))
router.start(App, '#app')

// just for debugging
window.router = router
