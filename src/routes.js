export default router => {
  router.map({
    '/': {
      component: {
        template: '<p>This is the contact page.</p>'
      }
    },
    '/grocery': {
      component: require('./components/Grocery/App.vue')
    }
  })
}
